#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
mod_doc_properties.py
Inkscape debug extension to modify document properties

Copyright (C) 2014 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import inkex

DEBUG = 0


def delNS(name):
    for tag, namespace in inkex.NSS.items():
        if namespace not in name:
            continue
        else:
            # FIXME: better substring extraction
            name = "%s:%s" % (tag, name[name.find("}")+1:])
    return name


class ModifyDocumentProperties(inkex.Effect):
    '''
    Modify document properties (<svg>, <sodipodi:namedview> attributes)
    '''

    def __init__(self):
        inkex.Effect.__init__(self)
        # Id
        self.OptionParser.add_option("--nv_id",
                                     action="store", type="string",
                                     dest="nv_id", default="base",
                                     help="Id of namedview node")
        # Size
        self.OptionParser.add_option("--nv_document_units",
                                     action="store", type="string",
                                     dest="nv_document_units", default="mm",
                                     help="Document default units")
        self.OptionParser.add_option("--nv_units",
                                     action="store", type="string",
                                     dest="nv_units", default="mm",
                                     help="Page units")
        self.OptionParser.add_option("--svg_viewbox",
                                     action="store", type="string",
                                     dest="svg_viewbox", default="",
                                     help="Page viewBox")
        self.OptionParser.add_option("--svg_width",
                                     action="store", type="float",
                                     dest="svg_width", default="210.0",
                                     help="Page width")
        self.OptionParser.add_option("--svg_height",
                                     action="store", type="float",
                                     dest="svg_height", default="297.0",
                                     help="Page height")
        # Resize margins
        self.OptionParser.add_option("--nv_fit_margin_top",
                                     action="store", type="float",
                                     dest="nv_fit_margin_top", default="0",
                                     help="Margin top")
        self.OptionParser.add_option("--nv_fit_margin_left",
                                     action="store", type="float",
                                     dest="nv_fit_margin_left", default="0",
                                     help="Margin left")
        self.OptionParser.add_option("--nv_fit_margin_right",
                                     action="store", type="float",
                                     dest="nv_fit_margin_right", default="0",
                                     help="Margin right")
        self.OptionParser.add_option("--nv_fit_margin_bottom",
                                     action="store", type="float",
                                     dest="nv_fit_margin_bottom", default="0",
                                     help="Margin bottom")
        # Display
        self.OptionParser.add_option("--nv_pagecolor",
                                     action="store", type="string",
                                     dest="nv_pagecolor", default="#ffffff",
                                     help="Page color")
        self.OptionParser.add_option("--nv_pageopacity",
                                     action="store", type="float",
                                     dest="nv_pageopacity", default="0.0",
                                     help="Page opacity")
        self.OptionParser.add_option("--nv_showborder",
                                     action="store", type="inkbool",
                                     dest="nv_showborder", default="True",
                                     help="Show Page border")
        self.OptionParser.add_option("--nv_bordercolor",
                                     action="store", type="string",
                                     dest="nv_bordercolor", default="#ffffff",
                                     help="Border color")
        self.OptionParser.add_option("--nv_borderopacity",
                                     action="store", type="float",
                                     dest="nv_borderopacity", default="0.0",
                                     help="Border opacity")
        self.OptionParser.add_option("--nv_borderlayer",
                                     action="store", type="inkbool",
                                     dest="nv_borderlayer", default="False",
                                     help="Border on top of drawing")
        self.OptionParser.add_option("--nv_showpageshadow",
                                     action="store", type="inkbool",
                                     dest="nv_showpageshadow", default="True",
                                     help="Show page shadow")
        self.OptionParser.add_option("--nv_pageshadow",
                                     action="store", type="string",
                                     dest="nv_pageshadow", default="2",
                                     help="Page shadow type")
        # Zoom
        self.OptionParser.add_option("--nv_zoom",
                                     action="store", type="float",
                                     dest="nv_zoom", default="0.35",
                                     help="Zoom level")
        self.OptionParser.add_option("--nv_cx",
                                     action="store", type="float",
                                     dest="nv_cx", default="400",
                                     help="View center x")
        self.OptionParser.add_option("--nv_cy",
                                     action="store", type="float",
                                     dest="nv_cy", default="560.0",
                                     help="View center y")
        # Guides
        self.OptionParser.add_option("--nv_showguides",
                                     action="store", type="inkbool",
                                     dest="nv_showguides", default="False",
                                     help="Show Guides")
        self.OptionParser.add_option("--nv_guidecolor",
                                     action="store", type="string",
                                     dest="nv_guidecolor", default="#0000ff",
                                     help="Guide color")
        self.OptionParser.add_option("--nv_guideopacity",
                                     action="store", type="float",
                                     dest="nv_guideopacity", default="0.4",
                                     help="Guide opacity")
        self.OptionParser.add_option("--nv_guidehicolor",
                                     action="store", type="string",
                                     dest="nv_guidehicolor", default="#ff0000",
                                     help="Guide hilight color")
        self.OptionParser.add_option("--nv_guidehiopacity",
                                     action="store", type="float",
                                     dest="nv_guidehiopacity", default="0.4",
                                     help="Guide hilight opacity")
        # Grids
        self.OptionParser.add_option("--nv_showgrid",
                                     action="store", type="inkbool",
                                     dest="nv_showgrid", default="False",
                                     help="Show Grids")
        # Snapping
        self.OptionParser.add_option("--nv_snap_global",
                                     action="store", type="inkbool",
                                     dest="nv_snap_global", default="True",
                                     help="Global snapping")
        self.OptionParser.add_option("--nv_snap_perpendicular",
                                     action="store", type="inkbool",
                                     dest="nv_snap_perpendicular", default="False",
                                     help="Snap perpendicularly")
        self.OptionParser.add_option("--nv_snap_tangential",
                                     action="store", type="inkbool",
                                     dest="nv_snap_tangential", default="False",
                                     help="Snap tangentially")
        # Misc
        self.OptionParser.add_option("--nv_current_layer",
                                     action="store", type="string",
                                     dest="nv_current_layer", default="layer1",
                                     help="Current layer (id)")
        # tabs
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab")
        # scope
        self.OptionParser.add_option("--nv_set_all",
                                     action="store", type="inkbool",
                                     dest="nv_set_all", default="False",
                                     help="Apply in all tabs")
        self.OptionParser.add_option("--nv_verbose",
                                     action="store", type="inkbool",
                                     dest="nv_verbose", default="False",
                                     help="Verbose mode")

    def print_attribute_info(self, attributes):
        inkex.debug("Number of attributes: %s\n" % len(attributes.keys()))
        self.print_attributes(attributes)

    def print_attributes(self, attributes):
        for name, value in sorted(attributes.items()):
            inkex.debug('%s = %r' % (delNS(name), value))
        inkex.debug("\n")

    def compare_attributes(self, old, new):
        for name in sorted(old.keys()):
            inkex.debug("%s = %r --> %r" % (delNS(name), old.get(name), new.get(name)))

    def set_attributes_viewbox(self, node, w, h, u, viewbox):
        if not viewbox:
            viewbox = "0 0 %.5f %.5f" % (w, h)
        node.set('width', "%.5f%s" % (w, u))
        node.set('height', "%.5f%s" % (h, u))
        node.set('viewBox', viewbox)

    def set_attributes_svgroot_viewbox(self):
        # FIXME: what if 'inkscape:document-units' ≠ 'units'?
        self.set_attributes_viewbox(self.document.getroot(),
                                    self.options.svg_width,
                                    self.options.svg_height,
                                    self.options.nv_units,
                                    self.options.svg_viewbox)

    def set_attributes_size(self, node):
        node.set(inkex.addNS('document-units', 'inkscape'),
                 self.options.nv_document_units)
        node.set('units',
                 self.options.nv_units)
        node.set('fit-margin-top',
                 str(round(self.options.nv_fit_margin_top, 2)))
        node.set('fit-margin-left',
                 str(round(self.options.nv_fit_margin_left, 2)))
        node.set('fit-margin-right',
                 str(round(self.options.nv_fit_margin_right, 2)))
        node.set('fit-margin-bottom',
                 str(round(self.options.nv_fit_margin_bottom, 2)))
        self.set_attributes_svgroot_viewbox()

    def set_attributes_display(self, node):
        node.set('pagecolor',
                 self.options.nv_pagecolor)
        node.set(inkex.addNS('pageopacity', 'inkscape'),
                 str(round(self.options.nv_pageopacity, 2)))
        node.set('showborder',
                 str(self.options.nv_showborder))
        node.set('bordercolor',
                 self.options.nv_bordercolor)
        node.set('borderopacity',
                 str(round(self.options.nv_borderopacity, 2)))
        node.set('borderlayer',
                 str(self.options.nv_borderlayer))
        node.set(inkex.addNS('showpageshadow', 'inkscape'),
                 str(self.options.nv_showpageshadow))
        node.set(inkex.addNS('pageshadow', 'inkscape'),
                 self.options.nv_pageshadow)

    def set_attributes_zoom(self, node):
        node.set(inkex.addNS('zoom', 'inkscape'),
                 str(round(self.options.nv_zoom, 3)))
        node.set(inkex.addNS('cx', 'inkscape'),
                 str(round(self.options.nv_cx, 2)))
        node.set(inkex.addNS('cy', 'inkscape'),
                 str(round(self.options.nv_cy, 2)))

    def set_attributes_guides(self, node):
        node.set('showguides',
                 str(self.options.nv_showguides))
        node.set('guidecolor',
                 self.options.nv_guidecolor)
        node.set('guideopacity',
                 str(round(self.options.nv_guideopacity, 2)))
        node.set('guidehicolor',
                 self.options.nv_guidehicolor)
        node.set('guidehiopacity',
                 str(round(self.options.nv_guidehiopacity, 2)))

    def set_attributes_grids(self, node):
        node.set('showgrid',
                 str(self.options.nv_showgrid))

    def set_attributes_snapping(self, node):
        node.set(inkex.addNS('snap-global', 'inkscape'),
                 str(self.options.nv_snap_global))
        node.set(inkex.addNS('snap-perpendicular', 'inkscape'),
                 str(self.options.nv_snap_perpendicular))
        node.set(inkex.addNS('snap-tangential', 'inkscape'),
                 str(self.options.nv_snap_tangential))

    def set_attributes_misc(self, node):
        node.set(inkex.addNS('current-layer', 'inkscape'),
                 self.options.nv_current_layer)

    def set_attributes_all(self, node):
        self.set_attributes_size(node)
        self.set_attributes_display(node)
        self.set_attributes_zoom(node)
        self.set_attributes_guides(node)
        self.set_attributes_grids(node)
        self.set_attributes_snapping(node)
        self.set_attributes_misc(node)

    def set_attributes(self, node):
        node.set('id', self.options.nv_id)
        if self.options.nv_set_all:
            self.set_attributes_all(node)
        elif self.options.tab == '"size_tab"':
            self.set_attributes_size(node)
        elif self.options.tab == '"display_tab"':
            self.set_attributes_display(node)
        elif self.options.tab == '"zoom_tab"':
            self.set_attributes_zoom(node)
        elif self.options.tab == '"guides_tab"':
            self.set_attributes_guides(node)
        elif self.options.tab == '"grids_tab"':
            self.set_attributes_grids(node)
        elif self.options.tab == '"snapping_tab"':
            self.set_attributes_snapping(node)
        elif self.options.tab == '"misc_tab"':
            self.set_attributes_misc(node)
        else:
            inkex.debug("Unknown tab.")

    def effect(self):

        global DEBUG
        if self.options.nv_verbose:
            DEBUG = 1
        else:
            DEBUG = 0

        nv = self.getNamedView()

        if DEBUG:
            nv_attributes_old = dict(nv.attrib)

        self.set_attributes(nv)

        if DEBUG:
            nv_attributes_new = dict(nv.attrib)
            self.compare_attributes(nv_attributes_old, nv_attributes_new)


if __name__ == '__main__':
    effect = ModifyDocumentProperties()
    effect.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
