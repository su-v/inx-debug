#!/usr/bin/env python
"""
add_comments - test reloading document with added comments

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


def is_group(node):
    """Check whether node is of type SVG group."""
    return node.tag == inkex.addNS('g', 'svg')


def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


class AddComments(inkex.Effect):
    """Effect-based class to debug restacking XML comments."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="append_selected",
                                     help="Selected action")

    def append_selected(self, id_list):
        """Insert a XML comment after each selected object."""
        for node_id in reversed(id_list):
            node = self.getElementById(node_id)
            parent = node.getparent()
            node_index = parent.index(node)
            comment = inkex.etree.Comment(text=node_id)
            parent.insert(node_index+1, comment)

    def prepend_selected(self, id_list):
        """Insert a XML comment before each selected object."""
        for node_id in reversed(id_list):
            node = self.getElementById(node_id)
            parent = node.getparent()
            node_index = parent.index(node)
            comment = inkex.etree.Comment(text=node_id)
            parent.insert(node_index, comment)

    def append_root(self):
        """Append new comment in SVG root at index -1."""
        root = self.document.getroot()
        comment = inkex.etree.Comment(text=root.get('id'))
        root.append(comment)

    def prepend_root(self):
        """Insert new comment in SVG root at index 0."""
        root = self.document.getroot()
        comment = inkex.etree.Comment(text=root.get('id'))
        root.insert(0, comment)

    def move_to_parent(self, id_list):
        """Move content from selected groups to parent."""
        for node_id in reversed(id_list):
            node = self.getElementById(node_id)
            if is_group(node):
                parent = node.getparent()
                node_index = parent.index(node)
                for child in node.iterchildren(reversed=True):
                    parent.insert(node_index, child)

    def move_to_root(self, id_list):
        """Move content from selected groups to parent."""
        root = self.document.getroot()
        for node_id in reversed(id_list):
            node = self.getElementById(node_id)
            if is_group(node):
                for child in node.iterchildren(reversed=False):
                    root.append(child)

    def ungroup(self, id_list):
        """Ungroup (move content, remove empty group).

        Simple version (does not preserve transforms, or style inherited from
        parent group).
        """
        for node_id in reversed(id_list):
            node = self.getElementById(node_id)
            if is_group(node):
                parent = node.getparent()
                node_index = parent.index(node)
                for child in node.iterchildren(reversed=True):
                    parent.insert(node_index, child)
                parent.remove(node)

    def delete_comments(self, id_list):
        """Delete comments inside selected group."""
        for node_id in reversed(id_list):
            node = self.getElementById(node_id)
            if is_group(node):
                for child in list(node.iterchildren(tag=inkex.etree.Comment)):
                    node.remove(child)

    def effect(self):
        """Main entry point to process current document."""
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        if self.options.action == 'prepend_selected':
            self.prepend_selected(id_list)
        elif self.options.action == 'append_selected':
            self.append_selected(id_list)
        elif self.options.action == 'prepend_root':
            self.prepend_root()
        elif self.options.action == 'append_root':
            self.append_root()
        elif self.options.action == 'move_to_parent':
            self.move_to_parent(id_list)
        elif self.options.action == 'move_to_root':
            self.move_to_root(id_list)
        elif self.options.action == 'ungroup_selected':
            self.ungroup(id_list)
        elif self.options.action == 'delete_comments':
            self.delete_comments(id_list)


if __name__ == '__main__':
    ME = AddComments()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
