#!/usr/bin/env python
"""
convert_rects - test changing objects with patched inkscape

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import simpletransform


__version__ = '0.0'


def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def is_rect(node):
    """Check whether node is SVG element type <rect/>."""
    return node.tag == inkex.addNS('rect', 'svg')


def draw_bbox(bbox):
    """Draw path with bbox coordinates (xmin,xMax,ymin,yMax)."""
    path_d = 'M {},{} {},{} {},{} {},{} Z'.format(bbox[0], bbox[2],
                                                  bbox[1], bbox[2],
                                                  bbox[1], bbox[3],
                                                  bbox[0], bbox[3])
    bbox_path = inkex.etree.Element(inkex.addNS('path', 'svg'))
    bbox_path.set('d', path_d)
    bbox_path.set('style', "fill:blue;fill-opacity:0.25;stroke:none")
    return bbox_path


def convert_rects(rectangles):
    """Convert rectangles to SVG path elements based on geom bbox."""
    for rect in rectangles:
        parent = rect.getparent()
        pos = parent.index(rect)
        path = draw_bbox(simpletransform.computeBBox([rect]))
        path.set('id', rect.get('id'))
        path.set('style', rect.get('style'))
        parent.insert(pos, path)
        parent.remove(rect)


class ConvertRects(inkex.Effect):
    """Effect-based class to debug changing existing objects."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="convert_rects",
                                     help="Selected action")

    def get_rects(self, id_list):
        """Return list of ids of rects in the current selection."""
        rectangles = []
        for node_id in id_list:
            node = self.getElementById(node_id)
            if node is not None:
                if is_rect(node):
                    rectangles.append(node)
        return rectangles

    def effect(self):
        """Main entry point to process current document."""
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        if self.options.mode == 'convert_rects':
            convert_rects(self.get_rects(id_list))


if __name__ == '__main__':
    ME = ConvertRects()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
