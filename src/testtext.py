#!/usr/bin/env python
"""
testtext - test text from input and SVG objects

Copyright (C) 2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=missing-docstring

# standard library
import sys
import locale
from datetime import date

# compat
import six

# local library
import inkex


if hasattr(inkex, 'localize'):
    inkex.localize()
else:
    import gettext  # pylint: disable=wrong-import-order
    _ = gettext.gettext


def report_args():
    inkex.debug("Length of list of command line arguments " +
                "passed to Python script: %s" % len(sys.argv))
    for arg in sys.argv:
        inkex.debug(arg)


def report_text(what, content):
    inkex.debug("\n=== origin: %s" % what)
    inkex.debug("=== type: %s" % type(content))
    inkex.debug("=== len: %s" % len(content))
    inkex.errormsg(content)
    # pylint: disable=undefined-variable
    if ((six.PY2 and isinstance(content, unicode)) or
            (six.PY3 and isinstance(content, str))):
        inkex.debug("=== unicode-escaped:")
        inkex.errormsg(content.encode('unicode_escape'))
    elif six.PY3 and isinstance(content, bytes):
        inkex.debug("=== bytes string:")
        inkex.debug(content)


def resolve_tspan(node):
    """Recursively process <tspan> and append text content."""
    line = ""
    if node.tag == inkex.addNS('tspan', 'svg'):
        if node.text:
            line += node.text
        for child in node:
            line += resolve_tspan(child)
        if node.tail:
            line += node.tail
    return line


def get_text_lines(node):
    """Extract text content of node and return list of strings."""
    lines = origin = []
    if node.tag == inkex.addNS('text', 'svg'):
        origin = [float(node.get('x', 0)), float(node.get('y', 0))]
        if node.text:
            lines.append(node.text)
        for child in node:
            lines.append(resolve_tspan(child))
    return (lines, origin)


class DebugTestText(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.encoding = None
        self.OptionParser.add_option("--basictest",
                                     action="store",
                                     type="inkbool",
                                     dest="basictest",
                                     default=False,
                                     help="")
        self.OptionParser.add_option("--reportargs",
                                     action="store",
                                     type="inkbool",
                                     dest="reportargs",
                                     default=False,
                                     help="")
        self.OptionParser.add_option("--reporttext",
                                     action="store",
                                     type="inkbool",
                                     dest="reporttext",
                                     default=False,
                                     help="")
        self.OptionParser.add_option("--input",
                                     action="store",
                                     type="string",
                                     dest="inputstring",
                                     default="Type your text here",
                                     help="")
        self.OptionParser.add_option("--aa",
                                     action="store",
                                     type="string",
                                     dest="option_aa",
                                     default="one",
                                     help="")

    def setencoding(self):
        """Set encoding for converting text to Unicode (for XML)."""
        self.encoding = sys.stdin.encoding
        # Fallback for Windows (see bug #1518302)
        if self.encoding == 'cp0' or self.encoding is None:
            # pylint: disable=redefined-variable-type
            if locale.getpreferredencoding():
                self.encoding = locale.getpreferredencoding()
            else:
                self.encoding = 'UTF-8'

    def to_unicode(self, astring, encoding=None):
        """Python 2: decode str (from input) to Unicode."""
        if encoding is None:
            encoding = self.encoding
        if sys.version_info < (3,):
            # Python 2
            return astring.decode(encoding)
        else:
            # Python 3
            return astring

    def test_basic(self):
        today = date.today()
        inkex.errormsg(_("Today is %s" % today))
        inkex.debug("Chosen enum: %s" % self.options.option_aa)
        inkex.debug("Number of selected objects: %s" % len(self.selected))

    def report_input_string(self):
        """Report type and content of input string(s)."""
        string_input = self.options.inputstring
        report_text('string_input', string_input)
        unicode_input = self.to_unicode(string_input)
        report_text('to_unicode(string_input)', unicode_input)
        if six.PY3:
            bytes_input = string_input.encode()
            report_text('string_input.encode()', bytes_input)

    def report_selected_text(self):
        """Iterate through selected text objects and process text content."""
        count = 0
        for id_, node in six.iteritems(self.selected):
            lines, _ = get_text_lines(node)
            if len(lines):
                count += 1
                for line in lines:
                    report_text("text object %s (%s)" % (str(count), id_), line)

    def effect(self):
        self.setencoding()
        if self.options.basictest:
            self.test_basic()
        if self.options.reportargs:
            report_args()
        if self.options.reporttext:
            inkex.debug("input/output encoding: %s" % self.encoding)
            self.report_input_string()
            self.report_selected_text()


if __name__ == '__main__':
    ME = DebugTestText()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
