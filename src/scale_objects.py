#!/usr/bin/env python
"""
scale_objects - test crash with patched build when previewing scaling of
                objects with custom cotation center, and SVG output pruning
                inappropriate style properties (e.g. font-related properties on
                a path).

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors
# pylint: disable=invalid-name

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def parameterize(x, a, b):
    """Parameterize x into interval defined by a and b."""
    return 0 if a == b else (x - a) / float(b - a)


def interpolate(t, a, b):
    """The reverse of parameterize()."""
    return a + float(b - a)*t


class ScaleObjects(inkex.Effect):
    """Effect-based class to debug crash with custom rotation centers."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="append_selected",
                                     help="Selected action")
        self.OptionParser.add_option("--scale_factor",
                                     action="store", type="float",
                                     dest="scale_factor", default=2.0,
                                     help="Selected action")

    def scale_selected(self, id_list, x=0, y=0):
        """Scale selected objects with interpolated factor."""
        ident = 1.0
        steps = len(id_list)
        start = ident
        end = self.options.scale_factor
        for i, node_id in enumerate(id_list):
            factor = interpolate(float(i+1)/steps, start, end)
            sx = x * factor or ident
            sy = y * factor or ident
            scaling = 'scale({},{})'.format(sx, sy)
            node = self.getElementById(node_id)
            transform = node.get('transform', '')
            node.set('transform', ' '.join([scaling, transform]))

    def effect(self):
        """Main entry point to process current document."""
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        if self.options.action == 'scale_xy_origin':
            self.scale_selected(id_list, x=1, y=1)
        elif self.options.action == 'scale_x_origin':
            self.scale_selected(id_list, x=1)
        elif self.options.action == 'scale_y_origin':
            self.scale_selected(id_list, y=1)


if __name__ == '__main__':
    ME = ScaleObjects()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
