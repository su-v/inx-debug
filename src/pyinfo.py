#!/usr/bin/env python
"""
Python module info - display system and python info

Note:
    This "debug" extension does not depend on inkex.py
    or other python modules usually required by Inkscape's
    python-based extension scripts: it tries to import
    required python modules and informs if this fails.

Copyright (C) 2010, Craig Marshall
Copyright (C) 2010-2016 su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=missing-docstring

# standard library
import sys
import os


def check_version(cur_ver, min_ver):
    """Check current version against min required version."""
    # TODO: simplify and generalize version check
    if len(cur_ver) >= 3 and len(min_ver) >= 3:
        if cur_ver[0] > min_ver[0]:
            return True
        elif cur_ver[0] == min_ver[0]:
            if cur_ver[1] > min_ver[1]:
                return True
            elif cur_ver[1] == min_ver[1]:
                if cur_ver[2] >= min_ver[2]:
                    return True
    return False


def errormsg(msg):
    if sys.version_info < (3,):
        if isinstance(msg, unicode):
            sys.stderr.write(
                (msg).encode('utf-8') + "\n")
        else:
            sys.stderr.write(
                (unicode(
                    (msg), "utf-8", errors='replace') + "\n").encode('utf-8'))
    else:
        sys.stderr.write('{0}\n'.format(msg))


def show(msg):  # print
    errormsg(msg)


def line():  # line
    errormsg("-"*40)


def dots(ints):  # dotted
    return ".".join([str(i) for i in ints if i != ""])


def system_info():
    paths = "\n\t" + "\n\t".join(sys.path)
    # environs = "\n\t" + "\n\t".join(os.environ)
    show("Default Encoding: " + sys.getdefaultencoding())
    line()
    show("Filesystem Encoding: " + sys.getfilesystemencoding())
    line()
    show("Platform: " + sys.platform)
    line()
    try:
        show("Windows Version: " + sys.getwindowsversion())
        line()
    except (TypeError, AttributeError):
        pass
    show("Python sys.version_info: %s" % dots(sys.version_info[:3]))
    show("Python sys.version: \n%s" % sys.version)
    line()
    show("Python sys.executable: \n\t%s" % sys.executable)
    show("Python sys.exec_prefix: \n\t %s" % sys.exec_prefix)
    show("Python sys.prefix: \n\t%s" % sys.prefix)
    line()
    show("sys.path: " + paths)


def lxml_info():
    try:
        import lxml
        from lxml import etree
        show("lxml path: %s" % lxml.__path__[0])
        show("LXML_VERSION: %s" % dots(etree.LXML_VERSION))
        show("LIBXML_VERSION: %s" % dots(etree.LIBXML_VERSION))
        show("LIBXML_COMPILED_VERSION: %s" % dots(
            etree.LIBXML_COMPILED_VERSION,))
        show("LIBXSLT_VERSION: %s" % dots(
            etree.LIBXSLT_VERSION))
        show("LIBXSLT_COMPILED_VERSION: %s" % dots(
            etree.LIBXSLT_COMPILED_VERSION))
    except ImportError:
        error_msg = sys.exc_info()[1]
        show("The fantastic lxml wrapper for libxml2" +
             "is required by this extension." +
             "\n\nTechnical details:\n%s" % (error_msg,))


def numpy_info():
    try:
        import numpy
        show("numpy path: %s" % numpy.__path__[0])
        show("numpy version: %s" % numpy.__version__)
    except ImportError:
        error_msg = sys.exc_info()[1]
        show("numpy is required by this extension." +
             "\n\nTechnical details:\n%s" % (error_msg,))


def wand_info():
    have_wand = False
    wand_min_req = (0, 4, 1)
    magick_home = os.environ.get('MAGICK_HOME')
    try:
        from wand.version import VERSION_INFO as wand_version_info
        if check_version(wand_version_info, wand_min_req):
            from wand.image import Image as ImageWand
            from wand.color import Color as ColorWand
            have_wand = True
    except ImportError:
        error_msg = sys.exc_info()[1]
        show("wand failed to import." +
             "\n\nTechnical details:\n%s" % (error_msg,))
    if have_wand:
        show("wand version: %s" % dots(wand_version_info))
        show("MAGICK_HOME is %s" % magick_home)


def environ_info():
    show("os.environ: ")
    for param in sorted(os.environ):
        show("%s \n\t%s" % (param, os.environ[param]))


def foo_info():
    try:
        from foo import bar
        show("bar from foo imported")
    except ImportError:
        show("bar from foo failed to load.")


class Effect(object):
    """A class for creating Inkscape SVG Effects"""

    def effect(self):
        pass

    def affect(self):
        self.effect()


class DebugInkscapeExtensions(Effect):

    def effect(self):
        system_info()
        line()
        lxml_info()
        line()
        numpy_info()
        line()
        wand_info()
        line()
        environ_info()
        line()
        foo_info()
        line()


if __name__ == '__main__':
    ME = DebugInkscapeExtensions()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
