#!/usr/bin/env python
"""
selected_nodes_modify - test processing selected nodes

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# standard library
import csv

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import cubicsuperpath
import interp


__version__ = '0.0'


def revsorted_set(alist):
    """Return sorted and reversed list of unique list members."""
    return sorted(set(alist), reverse=True)


class ModifySelectedNodes(inkex.Effect):
    """Effect-based class to debug modifying seleced nodes."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="list_selected",
                                     help="Selected action")

    # ----- modify nodes

    def insert_node_after(self, selnode):
        """Insert node in sub-path after selnode."""
        path = self.getElementById(selnode[0])
        subpath, index = selnode[1]
        csp = cubicsuperpath.parsePath(path.get('d'))
        pt1 = csp[subpath][index]
        pt2 = csp[subpath][index+1]
        newsegments = interp.cspbezsplitatlength(pt1, pt2, 0.5)
        if len(newsegments) == 3:
            csp[subpath][index:index+2] = newsegments[:]
        path.set('d', cubicsuperpath.formatPath(csp))
        path.attrib.pop(inkex.addNS('nodetypes', 'sodipodi'), None)

    def delete_node(self, selnode):
        """Delete selected node from path's csp."""
        path = self.getElementById(selnode[0])
        subpath, index = selnode[1]
        csp = cubicsuperpath.parsePath(path.get('d'))
        try:
            csp[subpath].pop(index)
        except IndexError:
            pass
        if any(len(subpath) for subpath in csp):
            path.set('d', cubicsuperpath.formatPath(csp))
            path.attrib.pop(inkex.addNS('nodetypes', 'sodipodi'), None)
        else:
            path.getparent().remove(path)

    def update_node_coords(self, selnode, pos):
        """Update position of selected node *selnode*."""
        path = self.getElementById(selnode[0])
        subpath, index = selnode[1]
        csp = cubicsuperpath.parsePath(path.get('d'))
        csp[subpath][index] = pos
        path.set('d', cubicsuperpath.formatPath(csp))

    def get_node_coords(self, selnode):
        """Return coordinates of selected node."""
        path = self.getElementById(selnode[0])
        subpath, index = selnode[1]
        csp = cubicsuperpath.parsePath(path.get('d'))
        return csp[subpath][index]

    def get_selected_nodes(self):
        """Return list with selected nodes in selection order."""
        # inkex.debug(self.options.selected_nodes)
        nodes = csv.reader(self.options.selected_nodes, delimiter=":")
        return ((node[0], tuple(int(i) for i in node[1:]))
                for node in nodes)

    # ----- actions

    def list_node_coords(self):
        """Return list of coordinates of selected nodes."""
        for i, selnode in enumerate(self.get_selected_nodes()):
            inkex.debug([i+1, self.get_node_coords(selnode)[1]])

    def delete_selected_nodes(self):
        """Delete selected nodes from path (not shape-preserving)."""
        nodes = list(self.get_selected_nodes())
        for path_id in set(node[0] for node in nodes):
            subpaths = [node[1] for node in nodes if node[0] == path_id]
            for subpath in revsorted_set(node[0] for node in subpaths):
                indices = [node[1] for node in subpaths if node[0] == subpath]
                for index in revsorted_set(indices):
                    selnode = (path_id, (subpath, index))
                    self.delete_node(selnode)

    def insert_between_selected_nodes(self):
        """Insert new node between two adjacent selected nodes."""
        nodes = list(self.get_selected_nodes())
        for path_id in set(node[0] for node in nodes):
            subpaths = [node[1] for node in nodes if node[0] == path_id]
            for subpath in revsorted_set(node[0] for node in subpaths):
                indices = [node[1] for node in subpaths if node[0] == subpath]
                for index in revsorted_set(indices):
                    if index-1 in indices:
                        selnode = (path_id, (subpath, index-1))
                        self.insert_node_after(selnode)

    def move_selected_nodes(self, delta_x=0.0, delta_y=0.0):
        """Move selected nodes down by fixed amount of user units."""
        for selnode in self.get_selected_nodes():
            pos = self.get_node_coords(selnode)
            for i in range(3):
                pos[i][0] += delta_x
                pos[i][1] += delta_y
            self.update_node_coords(selnode, pos)

    def list_selected_nodes(self):
        """Return list of selected nodes as passed by inkscape."""
        for i, selnode in enumerate(self.get_selected_nodes()):
            path, (subpath, index) = selnode
            inkex.debug('{} - {}: subpath: {} node: {}'.format(
                i, path, subpath, index))

    # ----- main

    def effect(self):
        """Main entry point to process current document."""
        step = 10.0
        if self.options.action == 'list_selected':
            self.list_selected_nodes()
        elif self.options.action == 'delete_selected_nodes':
            self.delete_selected_nodes()
        elif self.options.action == 'insert_between_selected_nodes':
            self.insert_between_selected_nodes()
        elif self.options.action == 'move_selected_nodes_down':
            self.move_selected_nodes(delta_y=step)


if __name__ == '__main__':
    ME = ModifySelectedNodes()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
