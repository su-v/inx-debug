#!/usr/bin/env python
"""
selection_sets - test working with inkscape tags for selection sets

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


NOTE:

    Inkscape's performance is abysmal (both wrt memory usage and
    speed) when reloading a file with a large selection set added.
    This needs upstream fix in core inkscape.

TODO:

    Possibly check and limit new sets to a tiny object count that is
    still handled ~ok by inkscape. It might be necessary to restrict
    the overall amount of tagrefs at any time in any given inkscape
    document though (needs further testing).

    Actually, it's all quite pointless - selection sets cannot be
    used for extensions later on anyway (inkscape crashes on reload).

"""
# pylint: disable=too-many-ancestors

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


SKIP_CONTAINERS = [
    'defs',
    'glyph',
    'marker',
    'mask',
    'missing-glyph',
    'pattern',
    'symbol',
]
CONTAINER_ELEMENTS = [
    'a',
    'g',
    'switch',
]
GRAPHICS_ELEMENTS = [
    'circle',
    'ellipse',
    'image',
    'line',
    'path',
    'polygon',
    'polyline',
    'rect',
    'text',
    'use',
]


def get_defs(node):
    """Find <defs> in children of *node*, return first one found."""
    path = '/svg:svg//svg:defs'
    try:
        return node.xpath(path, namespaces=inkex.NSS)[0]
    except IndexError:
        return inkex.etree.SubElement(node, inkex.addNS('defs', 'svg'))


def is_inkscape_tag(node):
    """Check whether node is Inkscape selection set (inkscape:tag)."""
    return node.tag == inkex.addNS('tag', 'inkscape')


def is_inkscape_tagref(node):
    """Check whether node is selection set reference (inkscape:tagref)."""
    return node.tag == inkex.addNS('tagref', 'inkscape')


def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def iter_graphic_elements(node):
    """Return descendents of node as iterator over of graphics elements."""
    graphics = [inkex.addNS(c, 'svg') for c in GRAPHICS_ELEMENTS]
    for element in node.iter(tag=inkex.etree.Element):
        if element.tag in graphics:
            yield element


def get_graphic_elements(node):
    """Return descendents of node as list of graphics element nodes."""
    return list(iter_graphic_elements(node))


def get_graphic_element_ids(node):
    """Return descendents of node as ID list of graphics elements."""
    return [element.get('id') for element in iter_graphic_elements(node)]


class SelectionSets(inkex.Effect):
    """Effect-based class to create and manage Inkscape selection sets."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # instance attributes
        self.selection_sets = {}

        # options
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="add",
                                     help="Mode (info, add)")

    def iter_inverted_selection(self):
        """Return iter over ids not part of current selection."""
        selected = self.selected.keys()
        parents = set()
        inverted = set()
        for node in self.selected.values():
            parents.update([node.getparent()])
        for node in parents:
            inverted.update(get_graphic_element_ids(node))
        for id_ in inverted:
            if id_ in selected:
                selected.remove(id_)
            else:
                yield id_

    def get_inverted_selection(self):
        """Return list of ids not part of current selection."""
        return list(self.iter_inverted_selection())

    def get_selection_sets(self):
        """Build dict with selection sets in current document."""
        root = self.document.getroot()
        defs = get_defs(root)
        tdict = {}
        for tag in defs.findall("inkscape:tag", namespaces=inkex.NSS):
            tag_id = tag.get('id')
            ref_nodes = {}
            for tagref in tag.findall("inkscape:tagref", namespaces=inkex.NSS):
                ref_id = tagref.get(inkex.addNS('href', 'xlink'))[1:]
                ref_node = root.find('.//*[@id="{}"]'.format(ref_id))
                ref_nodes[ref_id] = ref_node
            tdict[tag_id] = ref_nodes
        self.selection_sets = tdict
        return len(self.selection_sets)

    def selection_set_info(self, verbose=False, extended=False):
        """Return information about current selection sets."""
        tag_count = self.get_selection_sets()
        if tag_count and verbose:
            root = self.document.getroot()
            tdict = self.selection_sets
            info = []
            for tag_id in sorted(tdict.keys()):
                info.append('Selection Set (id="{}"): {} objects'.format(
                    tag_id, len(tdict[tag_id].keys())))
                if extended:
                    for ref_id in z_iter(root, tdict[tag_id].keys()):
                        ref_node = tdict[tag_id][ref_id]
                        info.append('--- {} (type: {})'.format(
                            ref_node.get('id'),
                            inkex.etree.QName(ref_node).localname))
                    info.append('')
            for line in info:
                inkex.debug(line)

    def add_selection_set(self, id_list):
        """Add ids from id_list as selection set to defs."""
        root = self.document.getroot()
        defs = get_defs(root)
        tag_id = self.uniqueId('set')
        tag = inkex.etree.Element(inkex.addNS('tag', 'inkscape'))
        tag.set('id', tag_id)
        tag.set(inkex.addNS('label', 'inkscape'), tag_id)
        for ref_id in id_list:
            tagref = inkex.etree.Element(inkex.addNS('tagref', 'inkscape'))
            tagref.set(inkex.addNS('href', 'xlink'), '#{}'.format(ref_id))
            tag.append(tagref)
        if len(tag):
            defs.append(tag)
        return len(tag)

    def effect(self):
        """Main entry point to process current document."""
        if self.options.mode == 'info':
            # return information about current selection sets
            self.selection_set_info(verbose=True, extended=True)
        elif self.options.mode == 'add':
            # create a set based on selection passed to extension
            inkex.debug('Selected: {}'.format(
                self.add_selection_set(self.options.ids)))
            self.selection_set_info()
        elif self.options.mode == 'add_inverted':
            # create a selection set of inverted selection
            inkex.debug('Selected: {}'.format(len(self.options.ids)))
            inkex.debug('Inverted: {}'.format(
                self.add_selection_set(self.iter_inverted_selection())))
            self.selection_set_info()
        elif self.options.mode == 'add_both':
            # create selection sets of current and inverted selection
            inkex.debug('Selected: {}'.format(
                self.add_selection_set(self.options.ids)))
            inkex.debug('Inverted: {}'.format(
                self.add_selection_set(self.iter_inverted_selection())))
            self.selection_set_info()


if __name__ == '__main__':
    ME = SelectionSets()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
