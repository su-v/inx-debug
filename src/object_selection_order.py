#!/usr/bin/env python
"""
object_selection_order - test preserving selection order

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# standard library
import copy
import random

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import simplestyle
import simpletransform


__version__ = '0.0'


def get_defs(node):
    """Find <defs> in children of *node*, return first one found."""
    path = '/svg:svg//svg:defs'
    try:
        return node.xpath(path, namespaces=inkex.NSS)[0]
    except IndexError:
        return inkex.etree.SubElement(node, inkex.addNS('defs', 'svg'))


def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def tab_name(name):
    """Workaround for Inkscape's notebook page parameter value."""
    return name.strip('"')


def create_text(label, color=None, fontsize=None):
    """Create and format text with content."""
    color = "red" if color is None else color
    fontsize = 10.0 if fontsize is None else fontsize
    # style
    sdict = {}
    sdict['fill'] = color
    sdict['font-family'] = "monospace"
    sdict['font-size'] = fontsize
    sdict['line-height'] = "1.0"
    sdict['text-anchor'] = "middle"
    sdict['text-align'] = "center"
    # text element
    text = inkex.etree.Element(inkex.addNS('text', 'svg'))
    text.set('style', simplestyle.formatStyle(sdict))
    text.set(inkex.addNS('space', 'xml'), "preserve")
    # tspan with label
    tspan = inkex.etree.Element(inkex.addNS('tspan', 'svg'))
    tspan.set('dy', str(fontsize / 3.0))
    tspan.text = label
    text.append(tspan)
    return text


def draw_text(label, params, parent, pos):
    """Insert text with label at pos in parent."""
    text = create_text(label, **params)
    text.set('x', str(pos[0]))
    text.set('y', str(pos[1]))
    parent.append(text)


class SelectionOrder(inkex.Effect):
    """Effect-based class to debug preserving selection order."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--slice_start",
                                     action="store", type="int",
                                     dest="slice_start", default=0,
                                     help="Selection slice start")
        self.OptionParser.add_option("--slice_end",
                                     action="store", type="int",
                                     dest="slice_end", default=1,
                                     help="Selection slice end")
        self.OptionParser.add_option("--slice_step",
                                     action="store", type="int",
                                     dest="slice_step", default=1,
                                     help="Selection slice step")
        self.OptionParser.add_option("--paintcolor",
                                     action="store", type="string",
                                     dest="paintcolor", default="#ffaaaa",
                                     help="Fill color paint for slice objects")
        # props tab
        self.OptionParser.add_option("--props_action",
                                     action="store", type="string",
                                     dest="props_action", default="move_first",
                                     help="Selected props action")
        # set tab
        self.OptionParser.add_option("--insert_at",
                                     action="store", type="int",
                                     dest="insert_at", default=0,
                                     help="Insert slice at position")
        self.OptionParser.add_option("--insert_random",
                                     action="store", type="inkbool",
                                     dest="insert_random", default=False,
                                     help="Insert slice at random position")
        self.OptionParser.add_option("--insert_copies",
                                     action="store", type="inkbool",
                                     dest="insert_copies", default=False,
                                     help="Insert copies of the slice")
        # number objects
        self.OptionParser.add_option("--number",
                                     action="store", type="inkbool",
                                     dest="number", default=True,
                                     help="Number index on-canvas")
        self.OptionParser.add_option("--textcolor",
                                     action="store", type="string",
                                     dest="textcolor", default="red",
                                     help="Text color")
        self.OptionParser.add_option("--fontsize",
                                     action="store", type="float",
                                     dest="fontsize", default=18.0,
                                     help="Font size")
        # other common options
        self.OptionParser.add_option("--verbose",
                                     action="store", type="inkbool",
                                     dest="verbose", default=False,
                                     help="Verbose mode")
        # notebooks
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab")

    def create_label(self, node_id, i):
        """Create index label for node_id."""
        # content, style
        label = str(i)
        base_px = '{}px'.format(self.options.fontsize)
        params = {'color': self.options.textcolor,
                  'fontsize': self.unittouu(base_px)}
        # compute position
        node = self.getElementById(node_id)
        parent = node.getparent()
        pos = [0.0, 0.0]
        node_bbox = simpletransform.computeBBox([node])
        if node_bbox is not None and len(node_bbox) == 4:
            # center of bounding box
            pos = [node_bbox[0] + (node_bbox[1] - node_bbox[0]) / 2.0,
                   node_bbox[2] + (node_bbox[3] - node_bbox[2]) / 2.0]
        # draw the text
        draw_text(label, params, parent, pos)

    def number(self, id_list):
        """Create text with index label for each selected node."""
        for i, node_id in enumerate(id_list):
            self.create_label(node_id, i)

    def move_to_defs(self, node_id):
        """Move object with node_id to defs."""
        root = self.document.getroot()
        defs = get_defs(root)
        node = self.getElementById(node_id)
        defs.append(node)

    def unhide(self, node_id, prop="display", val="none"):
        """Unhide selected object."""
        node = self.getElementById(node_id)
        sdict = simplestyle.parseStyle(node.get('style'))
        if prop in sdict and sdict[prop] == val:
            sdict.pop(prop, None)
            node.set('style', simplestyle.formatStyle(sdict))
        elif prop in node.attrib and node.attrib[prop] == val:
            node.attrib.pop(prop, None)

    def hide(self, node_id, prop="display", val="none"):
        """Hide selected object."""
        node = self.getElementById(node_id)
        sdict = simplestyle.parseStyle(node.get('style'))
        sdict[prop] = val
        node.set('style', simplestyle.formatStyle(sdict))

    def random_opacity(self, sdict):
        """Add some randomness to the change."""
        # pylint: disable=no-self-use
        val = random.uniform(0.6, 1.0)
        sdict['fill-opacity'] = val

    def paint(self, node_id, prop="fill", color="#ffaaaa", opacity=True):
        """Paint fill color of selected object."""
        node = self.getElementById(node_id)
        sdict = simplestyle.parseStyle(node.get('style'))
        sdict[prop] = color
        if opacity:
            self.random_opacity(sdict)
        node.set('style', simplestyle.formatStyle(sdict))

    def dirty(self):
        """Force reload of document by changing id of SVG root."""
        root = self.document.getroot()
        root.set('id', self.uniqueId('RootID'))

    def show_order(self, id_list, indices=None):
        """Return info about order of selected objects."""
        # pylint: disable=no-self-use
        indices = slice(0, 1, 1) if indices is None else indices
        inkex.debug(id_list)
        inkex.debug('Selection slice: {}'.format(indices))
        inkex.debug('Selection slice: {}'.format(id_list[indices]))

    def modify_props(self, id_list):
        """Modify attributes or properties of selected objects."""
        # pylint: disable=too-many-branches

        # get slice of selected objects (no checks for now)
        start = self.options.slice_start
        end = self.options.slice_end
        step = self.options.slice_step or 1
        indices = slice(start, end, step)
        sliced_list = list(id_list[indices])
        # booleans
        if self.options.verbose:
            self.show_order(id_list, indices)
        if self.options.number:
            self.number(id_list)
        # actions
        if self.options.props_action == 'color':
            for node_id in sliced_list:
                fill = self.options.paintcolor
                self.paint(node_id, color=fill)
        elif self.options.props_action == 'hide':
            for node_id in sliced_list:
                self.hide(node_id)
        elif self.options.props_action == 'unhide':
            for node_id in sliced_list:
                self.unhide(node_id)
        elif self.options.props_action == 'to_defs':
            for node_id in sliced_list:
                self.move_to_defs(node_id)
        elif self.options.props_action == 'debug':
            for node_id in sliced_list:
                inkex.debug(node_id)
        elif self.options.props_action == 'reload':
            self.dirty()
            self.show_order(id_list, indices)

    def copy(self, node):
        """Return a copy of node with node_id."""
        new_id = self.uniqueId(node.get('id'))
        new_node = copy.deepcopy(node)
        new_node.set('id', new_id)
        return new_node

    def insert_at(self, node_id, parent, pos):
        """Insert object with node_id at pos."""
        node = self.getElementById(node_id)
        if self.options.insert_copies:
            new_node = self.copy(node)
        else:
            new_node = node
        parent.insert(pos, new_node)
        fill = self.options.paintcolor
        self.paint(new_node.get('id'), color=fill, opacity=False)

    def modify_sets(self, id_list):
        """Re-arrange sub-sets of the selected objects."""
        # get slice of selected objects
        start = self.options.slice_start
        end = self.options.slice_end
        step = self.options.slice_step or 1
        indices = slice(start, end, step)
        sliced_list = list(id_list[indices])
        # target object
        if self.options.insert_random:
            insert_at = random.randint(0, len(id_list)-1)
        else:
            insert_at = self.options.insert_at
        if insert_at > 0:
            insert_at = min(insert_at, len(id_list)-1)
        elif insert_at < 0:
            insert_at = max(insert_at, len(id_list)-1)
        target_id = id_list[insert_at]
        target = self.getElementById(target_id)
        # target parent, target index in parent
        parent = target.getparent()
        target_index = parent.index(target)
        # insert set before target node
        for node_id in reversed(sliced_list):
            self.insert_at(node_id, parent, target_index)
        # booleans
        if self.options.verbose:
            self.show_order(id_list, indices)
        if self.options.number:
            self.number(id_list)

    def effect(self):
        """Main entry point to process current document."""
        # list of selected objects as passed from inkscape
        id_list = self.options.ids
        if not len(id_list):
            # without a selection, return early
            inkex.debug("Nothing selected.")
            return
        elif tab_name(self.options.tab) == 'props_tab':
            self.modify_props(id_list)
        elif tab_name(self.options.tab) == 'sets_tab':
            self.modify_sets(id_list)

    # ----- workaround to fix Effect() performance with large selections

    def collect_ids(self, doc=None):
        """Iterate all elements, build id dicts (doc_ids, selected)."""
        doc = self.document if doc is None else doc
        id_list = list(self.options.ids)
        for node in doc.getroot().iter(tag=inkex.etree.Element):
            if 'id' in node.attrib:
                node_id = node.get('id')
                self.doc_ids[node_id] = 1
                if node_id in id_list:
                    self.selected[node_id] = node
                    id_list.remove(node_id)

    def getselected(self):
        """Overload Effect() method."""
        self.collect_ids()

    def getdocids(self):
        """Overload Effect() method."""
        pass


if __name__ == '__main__':
    ME = SelectionOrder()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
