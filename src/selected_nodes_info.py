#!/usr/bin/env python
"""
selected_nodes_info - test processing selected nodes

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# standard library
import csv

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import cubicsuperpath
import simplestyle


__version__ = '0.0'


class SelectedNodesInfo(inkex.Effect):
    """Effect-based class to debug selected nodes."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="list_selected",
                                     help="Selected action")

    # ----- canvas output

    def create_text(self, label, color="red"):
        """Create and format text with content."""
        base_px = 18.0
        base_uu = self.unittouu("{}px".format(base_px))
        # style
        sdict = {}
        sdict['fill'] = color
        sdict['font-family'] = "monospace"
        sdict['font-size'] = base_uu
        sdict['line-height'] = "1.0"
        sdict['text-anchor'] = "start"
        sdict['text-align'] = "start"
        # text element
        text = inkex.etree.Element(inkex.addNS('text', 'svg'))
        text.set('style', simplestyle.formatStyle(sdict))
        text.set(inkex.addNS('space', 'xml'), "preserve")
        # tspan with label
        tspan = inkex.etree.Element(inkex.addNS('tspan', 'svg'))
        tspan.set('dy', str(base_uu / 3.0))
        tspan.text = " " + label
        text.append(tspan)
        return text

    def draw_text(self, pos, label, sibling):
        """Insert text with label at pos after sibling."""
        text = self.create_text(label)
        text.set('x', str(pos[0]))
        text.set('y', str(pos[1]))
        parent = sibling.getparent()
        parent.insert(parent.index(sibling)+1, text)

    # ----- query nodes

    def get_node_coords(self, selnode):
        """Return coordinates of selected node."""
        path = self.getElementById(selnode[0])
        subpath, index = selnode[1]
        csp = cubicsuperpath.parsePath(path.get('d'))
        return csp[subpath][index][1]

    def get_selected_nodes(self):
        """Return list with selected nodes in selection order."""
        # inkex.debug(self.options.selected_nodes)
        nodes = csv.reader(self.options.selected_nodes, delimiter=":")
        return ((node[0], tuple(int(i) for i in node[1:]))
                for node in nodes)

    # ----- actions

    def list_node_coords(self):
        """Return list of coordinates of selected nodes."""
        for i, selnode in enumerate(self.get_selected_nodes()):
            inkex.debug([i+1, self.get_node_coords(selnode)])

    def number_path_direction(self):
        """Number selected nodes per path in path direction."""
        nodes = list(self.get_selected_nodes())
        for path_id in set(node[0] for node in nodes):
            subpaths = [node[1] for node in nodes if node[0] == path_id]
            for subpath in set(node[0] for node in subpaths):
                indices = [node[1] for node in subpaths if node[0] == subpath]
                for index in indices:
                    coords = self.get_node_coords((path_id, (subpath, index)))
                    label = str(subpath) + ":" + str(index)
                    self.draw_text(coords, label, self.getElementById(path_id))

    def number_sort_min_max_x(self):
        """Number selected nodes based on canvas position."""
        coords = [(self.get_node_coords(selnode), selnode[0])
                  for selnode in self.get_selected_nodes()]
        for i, (coords, path_id) in enumerate(sorted(coords)):
            self.draw_text(coords, str(i + 1), self.getElementById(path_id))

    def number_selection_order(self):
        """Number selected nodes on-canvas."""
        for i, selnode in enumerate(self.get_selected_nodes()):
            coords = self.get_node_coords(selnode)
            self.draw_text(coords, str(i + 1), self.getElementById(selnode[0]))

    def list_selected_nodes(self):
        """Return list of selected nodes as passed by inkscape."""
        for i, selnode in enumerate(self.get_selected_nodes()):
            path, (subpath, index) = selnode
            inkex.debug('{} - {}: subpath: {} node: {}'.format(
                i, path, subpath, index))

    # ----- main

    def effect(self):
        """Main entry point to process current document."""
        if self.options.action == 'list_selected':
            self.list_selected_nodes()
        elif self.options.action == 'number_selection_order':
            self.number_selection_order()
        elif self.options.action == 'number_sort_min_max_x':
            self.number_sort_min_max_x()
        elif self.options.action == 'number_path_direction':
            self.number_path_direction()
        elif self.options.action == 'coords_selected':
            self.list_node_coords()


if __name__ == '__main__':
    ME = SelectedNodesInfo()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
