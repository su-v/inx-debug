#!/usr/bin/env python
"""
mirror - test finding matrix to reflect objects across line

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=invalid-name

# standard library
import copy
from math import cos, sin, atan2  # , radians, degrees

# local library
import cubicsuperpath
try:
    import inkex_local as inkex
except ImportError:
    import inkex
from simpletransform import applyTransformToNode, applyTransformToPath
from simpletransform import composeParents, composeTransform, invertTransform
from simpletransform import parseTransform


__version__ = '0.0'


# ----- extended simpletransform.py

# pylint: disable=missing-docstring

def ident_mat():
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def rootToNodeTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return invertTransform(composeParents(node, mat))
    else:
        return mat


def nodeToRootTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return composeParents(node, mat)
    else:
        return mat


def nodeTransform(node):
    if 'transform' in node.attrib:
        return parseTransform(node.get('transform'))
    else:
        return ident_mat()


def composeTransformInRoot(node, center, mat_transform):
    """Compose mat_transform for node applied in SVG root."""
    node_mat = nodeTransform(node)
    root_mat = rootToNodeTransform(node)
    if center is None:
        center_mat = ident_mat()
    else:
        center_mat = [[1.0, 0.0, center[0]],
                      [0.0, 1.0, center[1]]]
    return composeTransform(
        node_mat,
        composeTransform(
            root_mat,
            composeTransform(
                center_mat,
                composeTransform(
                    mat_transform,
                    composeTransform(
                        invertTransform(center_mat),
                        composeTransform(
                            invertTransform(root_mat),
                            invertTransform(node_mat)))))))

# pylint: enable=missing-docstring


# ----- SVG helper functions

def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def make_copy(node, mode="duplicate"):
    """Create a copy of node.

    Return the original or the new node based on *mode*.
    """
    parent = node.getparent()
    index = parent.index(node)
    new_node = copy.deepcopy(node)
    new_node.attrib.pop('id', None)
    if mode == 'drop':
        # insert copy before, return original
        parent.insert(index, new_node)
        return node
    elif mode == 'stamp':
        # insert copy after, return copy
        parent.insert(index+1, new_node)
        return new_node
    else:
        # append copy to parent, return copy
        parent.append(new_node)
        return new_node


def make_clone(node, mode="stamp"):
    """Clone object with node_id *count* times."""
    parent = node.getparent()
    index = parent.index(node)
    href = '#{}'.format(node.get('id'))
    new_node = inkex.etree.Element(inkex.addNS('use', 'svg'))
    new_node.set(inkex.addNS('href', 'xlink'), href)
    if mode == 'drop':
        # insert clone before, return original
        parent.insert(index, new_node)
        return node
    elif mode == 'stamp':
        # insert clone after, return clone
        parent.insert(index+1, new_node)
        return new_node
    else:
        # append clone to parent, return clone
        parent.append(new_node)
        return new_node


# ----- mirror line

def is_path(node):
    """Check whether node is of type SVG path."""
    return node.tag == inkex.addNS('path', 'svg')


def get_points(node):
    """Return start and end node of *node* as pt1, pt2 in root."""
    csp = cubicsuperpath.parsePath(node.get('d'))
    mat = composeTransform(nodeToRootTransform(node), nodeTransform(node))
    applyTransformToPath(mat, csp)
    return [csp[0][0][1][:], csp[-1][-1][1][:]]


def slope(pt1, pt2):
    """Return slope of line pt1, pt2."""
    dx = pt2[0] - pt1[0]
    dy = pt2[1] - pt1[1]
    return dy / dx


def angle_x(pt1, pt2):
    """Return angle to x-axis of line pt1, pt2."""
    dx = pt2[0] - pt1[0]
    dy = pt2[1] - pt1[1]
    return atan2(dy, dx)


def angle_y(pt1, pt2):
    """Return angle to y-axis of line pt1, pt2."""
    dx = pt2[0] - pt1[0]
    dy = pt2[1] - pt1[1]
    return atan2(dx, dy)


def intercept_x(pt1, pt2):
    """Return x coordinate of line pt1, pt2 intercept with x-axis."""
    return pt1[0] - (pt1[1] / slope(pt1, pt2))


def intercept_y(pt1, pt2):
    """Return y coordinate of line pt1, pt2 intercept with y-axis."""
    return pt1[1] - (pt1[0] * slope(pt1, pt2))


# ----- matrices

def mat_translate(tx, ty):
    """Return matrix to translate by tx, ty."""
    return [[1.0, 0.0, tx],
            [0.0, 1.0, ty]]


def mat_reflect(phi):
    """Return reflection matrix for angle phi."""
    return [[cos(2*phi), sin(2*phi), 0.0],
            [sin(2*phi), -cos(2*phi), 0.0]]


# ----- main class

class Mirror(inkex.Effect):
    """Effect-based class to find reflection matrix."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        self.OptionParser.add_option("--apply_to_copy",
                                     action="store", type="inkbool",
                                     dest="apply_to_copy", default=True,
                                     help="Apply to copy")
        self.OptionParser.add_option("--copy_mode",
                                     action="store", type="string",
                                     dest="copy_mode", default="drop",
                                     help="Copy mode")
        self.OptionParser.add_option("--apply_to_clone",
                                     action="store", type="inkbool",
                                     dest="apply_to_clone", default=False,
                                     help="Clone instead of copy")
        self.OptionParser.add_option("--clone_mode",
                                     action="store", type="string",
                                     dest="clone_mode", default="stamp",
                                     help="Clone mode")
        self.OptionParser.add_option("--extension",
                                     action="store", type="string",
                                     dest="extension",
                                     default="mirror_objects",
                                     help="[hidden] extension parameter")

    # ----- mirror

    def transform(self, mat, node):
        """Apply final transformation matrix to object, or a copy/clone."""
        if self.options.apply_to_copy:
            if self.options.apply_to_clone:
                copy_node = make_clone(node, self.options.clone_mode)
            else:
                copy_node = make_copy(node, self.options.copy_mode)
            applyTransformToNode(mat, copy_node)
        else:
            applyTransformToNode(mat, node)

    def reflect(self, line, node_id):
        """Reflect node across mirror line."""
        node = self.selected[node_id]
        # mirror line
        phi = angle_x(*line)
        tx = intercept_x(*line)
        # anchor (offset of x-intercept to origin)
        anchor = (tx, 0)
        # transform to root, reflect, transform back to node
        mat_mirror = mat_reflect(phi)
        mat = composeTransformInRoot(node, anchor, mat_mirror)
        self.transform(mat, node)

    def mirror_objects(self):
        """Main entry for 'Mirror' extension."""
        id_list = z_sort(self.document.getroot(), self.selected.keys())
        if id_list is not None and len(id_list) > 1:
            top_path = self.selected[id_list.pop(-1)]
            if is_path(top_path):
                line = get_points(top_path)
                for node_id in id_list:
                    self.reflect(line, node_id)

    # ----- main

    def effect(self):
        """Main entry point to process current document."""
        func = getattr(self, self.options.extension, None)
        if func is not None:
            return func()
        else:
            inkex.debug('{} not found.'.format(self.options.extension))


if __name__ == '__main__':
    ME = Mirror()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
