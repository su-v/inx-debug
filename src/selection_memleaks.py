#!/usr/bin/env python
"""
selection_memleaks - investigate memleaks observed when previewing or
                     applying extensions with (larger) retained selections

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# standard libraries
import random

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
import simplestyle
import simpletransform


__version__ = '0.0'


TESTGROUP = "playground"


# ----- SVG helper functions

def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def get_namedview(doc):
    """Return <namedview/> document node in sodipodi namespace."""
    if doc is not None:
        path = '//sodipodi:namedview'
        result = doc.xpath(path, namespaces=inkex.NSS)
        if result:
            return result[0]
        else:
            return None


# ----- attribute value helper functions

def random_grey():
    """Return random greyscale color value."""
    return "rgb({0},{0},{0})".format(random.randint(64, 192))


def random_opacity(sdict):
    """Add some randomness to opacity in style dict."""
    val = random.uniform(0.6, 1.0)
    sdict['fill-opacity'] = val


# ----- create SVG objects

def create_text(label, color=None, fontsize=None):
    """Create and format text with content."""
    color = "red" if color is None else color
    fontsize = 10.0 if fontsize is None else fontsize
    # style
    sdict = {}
    sdict['fill'] = color
    sdict['font-family'] = "monospace"
    sdict['font-size'] = fontsize
    sdict['line-height'] = "1.0"
    sdict['text-anchor'] = "middle"
    sdict['text-align'] = "center"
    # text element
    text = inkex.etree.Element(inkex.addNS('text', 'svg'))
    text.set('style', simplestyle.formatStyle(sdict))
    text.set(inkex.addNS('space', 'xml'), "preserve")
    # tspan with label
    tspan = inkex.etree.Element(inkex.addNS('tspan', 'svg'))
    tspan.set('dy', str(fontsize / 3.0))
    tspan.text = label
    text.append(tspan)
    return text


def draw_text(label, params, parent, pos):
    """Insert text with label at pos in parent."""
    text = create_text(label, **params)
    text.set('x', str(pos[0]))
    text.set('y', str(pos[1]))
    parent.append(text)
    return text


def draw_rect(parent, node_id, position, size):
    """Create a rect object with node_id, size at position."""
    rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
    rect.set('id', node_id)
    rect.set('width', str(size))
    rect.set('height', str(size))
    rect.set('x', str(position[0]))
    rect.set('y', str(position[1]))
    rect.set('ry', str(size / 4.0))
    rect.set('style', "fill:{};stroke:none".format(random_grey()))
    parent.append(rect)
    return rect


def draw_group(parent, node_id):
    """Create group with node_id in current layer."""
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    group.set('id', node_id)
    parent.append(group)
    return group


# ----- upstream workarounds

def tab_name(name):
    """Workaround for Inkscape's notebook page parameter value."""
    return name.strip('"')


# ----- main class

class ModifySelected(inkex.Effect):
    """Effect-based class to debug memleaks with script extensions."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # common actions
        # create objects
        self.OptionParser.add_option("--create_action",
                                     action="store", type="string",
                                     dest="create_action",
                                     default="create_objects",
                                     help="Selected Create action")
        self.OptionParser.add_option("--create_rows",
                                     action="store", type="int",
                                     dest="create_rows", default=20,
                                     help="Number of rows in test array")
        self.OptionParser.add_option("--create_columns",
                                     action="store", type="int",
                                     dest="create_columns", default=20,
                                     help="Number of columns in test array")
        self.OptionParser.add_option("--create_size",
                                     action="store", type="int",
                                     dest="create_size", default=10,
                                     help="Size of test squares")
        # test actions
        self.OptionParser.add_option("--test_action",
                                     action="store", type="string",
                                     dest="test_action", default="none",
                                     help="Selected Test action")
        self.OptionParser.add_option("--paintcolor",
                                     action="store", type="string",
                                     dest="paintcolor", default="#ffaaaa",
                                     help="Fill color paint for slice objects")
        # number objects
        self.OptionParser.add_option("--number",
                                     action="store", type="inkbool",
                                     dest="number", default=True,
                                     help="Number index on-canvas")
        self.OptionParser.add_option("--textcolor",
                                     action="store", type="string",
                                     dest="textcolor", default="red",
                                     help="Text color")
        self.OptionParser.add_option("--fontsize",
                                     action="store", type="float",
                                     dest="fontsize", default=12.0,
                                     help="Font size")
        # notebooks
        self.OptionParser.add_option("--tab",
                                     action="store", type="string",
                                     dest="tab",
                                     help="The selected UI-tab")

    # ----- workaround to fix Effect() performance with large selections

    def collect_ids(self, doc=None):
        """Iterate all elements, build id dicts (doc_ids, selected)."""
        doc = self.document if doc is None else doc
        id_list = list(self.options.ids)
        for node in doc.getroot().iter(tag=inkex.etree.Element):
            if 'id' in node.attrib:
                node_id = node.get('id')
                self.doc_ids[node_id] = 1
                if node_id in id_list:
                    self.selected[node_id] = node
                    id_list.remove(node_id)

    def getselected(self):
        """Overload Effect() method."""
        self.collect_ids()

    def getdocids(self):
        """Overload Effect() method."""
        pass

    # ----- create actions

    def child_ids(self, group_id):
        """Return list of child ids in group with group_id."""
        group = self.getElementById(group_id)
        group_ids = []
        if group is not None:
            for child in group:
                group_ids.append(child.get('id'))
        return group_ids

    def create_array(self, parent, group_id):
        """Create array of rects ordered in columns x rows grid."""
        rows = self.options.create_rows
        columns = self.options.create_columns
        size = self.options.create_size
        group = draw_group(parent, group_id)
        for i in range(columns):
            for j in range(rows):
                rect_id = 'r_{:03}_{:03}'.format(i, j)
                pos_x = i * size
                pos_y = j * size
                draw_rect(group, rect_id, (pos_x, pos_y), size)
        namedview = get_namedview(self.document)
        namedview.set(inkex.addNS('current-layer', 'inkscape'), group_id)
        return True if len(group) else False

    def create_objects(self):
        """Create group of test objects for default test case."""
        # actions
        retval = False
        if self.options.create_action == 'create_objects':
            retval = self.create_array(self.current_layer, TESTGROUP)
        return retval

    # ----- number

    def create_label(self, node_id, i):
        """Create index label for node_id."""
        # content, style
        label = str(i)
        base_px = '{}px'.format(self.options.fontsize)
        params = {'color': self.options.textcolor,
                  'fontsize': self.unittouu(base_px)}
        # compute position
        node = self.getElementById(node_id)
        parent = node.getparent()
        pos = [0.0, 0.0]
        node_bbox = simpletransform.computeBBox([node])
        if node_bbox is not None and len(node_bbox) == 4:
            # center of bounding box
            pos = [node_bbox[0] + (node_bbox[1] - node_bbox[0]) / 2.0,
                   node_bbox[2] + (node_bbox[3] - node_bbox[2]) / 2.0]
        # draw the text
        draw_text(label, params, parent, pos)

    def number(self, id_list):
        """Create text with index label for each selected node."""
        for i, node_id in enumerate(id_list):
            self.create_label(node_id, i)

    # ----- test actions

    def paint(self, node_id, prop="fill", color="#ffaaaa", opacity=True):
        """Paint fill color of selected object."""
        node = self.getElementById(node_id)
        sdict = simplestyle.parseStyle(node.get('style'))
        sdict[prop] = color
        if opacity:
            random_opacity(sdict)
        node.set('style', simplestyle.formatStyle(sdict))

    def modify_props(self, id_list):
        """Modify attributes or properties of selected objects."""
        for node_id in id_list:
            fill = self.options.paintcolor
            self.paint(node_id, color=fill)
        return True

    def modify_objects(self):
        """Modify objects."""
        # list of selected objects in selection order
        id_list = self.options.ids
        # list of objects in test group
        group_ids = self.child_ids(TESTGROUP)

        # booleans
        if self.options.number and len(id_list):
            self.number(id_list)

        # modify objects
        retval = False
        # actions
        if self.options.test_action.endswith('in_group'):
            if len(group_ids) and len(id_list):
                inkex.debug("Please deselect everything first.")
            elif len(group_ids):
                if self.options.test_action.startswith('modify_all'):
                    retval = self.modify_props(group_ids)
                elif self.options.test_action.startswith('modify_one'):
                    random_id = group_ids[random.randint(0, len(group_ids)-1)]
                    retval = self.modify_props([random_id])
            else:
                inkex.debug("Please create test objects first.")
        elif self.options.test_action.endswith('in_selected'):
            if not len(id_list):
                inkex.debug("Nothing selected.")
            elif len(id_list):
                if self.options.test_action.startswith('modify_all'):
                    retval = self.modify_props(id_list)
                elif self.options.test_action.startswith('modify_one'):
                    random_id = id_list[random.randint(0, len(id_list)-1)]
                    retval = self.modify_props([random_id])
        return retval

    # ----- main

    def effect(self):
        """Main entry point to process current document."""
        if tab_name(self.options.tab) == 'objects_tab':
            return self.create_objects()
        elif tab_name(self.options.tab) == 'tests_tab':
            return self.modify_objects()


if __name__ == '__main__':
    ME = ModifySelected()
    if ME.affect():
        exit(0)
    else:
        exit(1)

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
