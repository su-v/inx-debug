#!/usr/bin/env python
"""
myTest - template Inkscape test extension

Copyright (C) 2012 ~suv, suv-sf@users.sourceforge.net

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# system library
from datetime import date
import sys

# local library
import inkex

try:
    inkex.localize()
except:
    inkex.errormsg("inkex.localize() failed.\n")
    import gettext
    _ = gettext.gettext


class testDebug(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("--aa",
                                     action="store", type="string",
                                     dest="option_aa", default="one",
                                     help="")

    def effect(self):
        today = date.today()
        inkex.errormsg(_("Today is %s" % today))
        inkex.errormsg(_("Number of selected objects: %s" % len(self.selected)))

        # inkex.errormsg(_("Length of list of command line arguments passed to Python script: %s"
        #                  % len(sys.argv)))
        # for ele in sys.argv:
        #     inkex.debug(ele)

        inkex.debug(_("Chosen enum: %s" % self.options.option_aa))

if __name__ == '__main__':
    e = testDebug()
    e.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
