<?xml version="1.0" encoding="UTF-8"?>
<!-- file: inx-dummy.inx -->
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">

  <name>INX Bug Tester</name>
  <id>su-v/org.inkscape.debug.INX</id>

  <!-- hidden param works fine here -->
  <param gui-hidden="true" name="hidden_opt" type="string">true</param>

  <param name="tab" type="notebook">

    <!-- string/int/float -->
    <page name="input_tab" _gui-text="Input">
      <!-- string -->
      <param name="param11" type="description" appearance="header">String</param>
      <param name="param11a" type="string" _gui-text="String:">name</param>
      <!-- int -->
      <param name="param12" type="description" appearance="header">Int</param>
      <param name="param12a" type="int" min="0" max="10" _gui-text="Int (default):">3</param>
      <param name="param12b" type="int" min="0" max="10" _gui-text="Int (full):" appearance="full">3</param>
      <!-- float -->
      <param name="param13" type="description" appearance="header">Float</param>
      <param name="param13a" type="float" max="50.0" min="-50.0" precision="3" _gui-text="Float (default):">12.34</param>
      <param name="param13b" type="float" max="50.0" min="-50.0" precision="3" _gui-text="Float (full):" appearance="full">12.34</param>
    </page>

    <!-- boolean/optiongroup/enum -->
    <page name="options_tab" _gui-text="Options">
      <!-- boolean -->
      <_param name="param14" type="description" appearance="header">Boolean</_param>
      <param name="param14a" type="boolean" _gui-text="Boolean parameter 1">true</param>
      <param name="param14b" type="boolean" _gui-text="Boolean parameter 2">false</param>
      <!-- optiongroup -->
      <_param name="param15" type="description" appearance="header">Optiongroup</_param>
      <param name="param15a" type="optiongroup" _gui-text="Optiongroup (default):">
        <_option value="opt11">Choice Number 1</_option>
        <_option value="opt12">Choice Number 2</_option>
        <_option value="opt13" default="true">Choice Number 3</_option>
        <_option value="opt14">Choice Number 4</_option>
      </param>
      <param name="param15b" type="optiongroup" _gui-text="Optiongroup (minimal):" appearance="minimal">
        <_option value="opt15">Choice Number 5</_option>
        <_option value="opt16">Choice Number 6</_option>
        <_option value="opt17" default="true">Choice Number 7</_option>
        <_option value="opt18">Choice Number 8</_option>
      </param>
      <!-- enum -->
      <_param name="param16" type="description" appearance="header">Enum</_param>
      <param name="param16a" type="enum" _gui-text="Enumeration (default):">
        <_item value="enum11">Choice Number 1</_item>
        <_item value="enum12">Choice Number 2</_item>
        <_item value="enum13" default="true">Choice Number 3</_item>
        <_item value="enum14">Choice Number 4</_item>
      </param>
    </page>

    <!-- layout tab -->
    <page name="layout_tab" _gui-text="Layout">
      <!-- header -->
      <param name="param31" type="description" appearance="header">Indent description</param>
      <!-- misc -->
      <param indent="0" name="param31a" type="description" xml:space="preserve">Text level 0</param>
      <param indent="2" name="param31b" type="description" xml:space="preserve">Text level 2</param>
      <param indent="4" name="param31c" type="description" xml:space="preserve">Text level 4</param>
      <!-- header -->
      <param name="param32" type="description" appearance="header">Indent string</param>
      <!-- misc -->
      <param indent="0" name="param32a" type="string" gui-text="Label string:">String level 0</param>
      <param indent="2" name="param32b" type="string" gui-text="Label string:">String level 2</param>
      <param indent="4" name="param32c" type="string" gui-text="Label string:">String level 4</param>
      <!-- header -->
      <param name="param33" type="description" appearance="header">Indent integer</param>
      <!-- misc -->
      <param indent="0" name="param33a" type="int" gui-text="Label int:">0</param>
      <param indent="2" name="param33b" type="int" gui-text="Label int:">2</param>
      <param indent="4" name="param33c" type="int" gui-text="Label int:">4</param>
      <!-- header -->
      <param name="param34" type="description" appearance="header">Indent integer (full)</param>
      <!-- misc -->
      <param indent="0" name="param34a" type="int" gui-text="Label int:" appearance="full">10</param>
      <param indent="2" name="param34b" type="int" gui-text="Label int:" appearance="full">8</param>
      <param indent="4" name="param34c" type="int" gui-text="Label int:" appearance="full">6</param>
      <!-- header -->
      <param name="param35" type="description" appearance="header">Indent enumeration</param>
      <!-- misc -->
      <param indent="0" name="param35a" type="enum" _gui-text="Enumeration (default):">
        <_item value="enum35a">Choice 1</_item>
        <_item value="enum35b">Choice 2</_item>
        <_item value="enum35c" default="true">Choice 3</_item>
        <_item value="enum35d">Choice 4</_item>
      </param>
      <param indent="2" name="param35b" type="enum" _gui-text="Enumeration (default):">
        <_item value="enum35e">Choice 1</_item>
        <_item value="enum35f">Choice 2</_item>
        <_item value="enum35g" default="true">Choice 3</_item>
        <_item value="enum35h">Choice 4</_item>
      </param>
      <param indent="4" name="param35c" type="enum" _gui-text="Enumeration (default):">
        <_item value="enum35i">Choice 1</_item>
        <_item value="enum35j">Choice 2</_item>
        <_item value="enum35k" default="true">Choice 3</_item>
        <_item value="enum35l">Choice 4</_item>
      </param>
    </page>

    <!-- layout tab -->
    <page name="layout_mixed_tab" _gui-text="Layout (mixed)">
      <!-- header -->
      <param indent="0" name="param21" type="description" appearance="header">Indent Header 0</param>
      <!-- misc -->
      <param indent="0" name="param21a" type="description" xml:space="preserve">Text level 0</param>
      <param indent="0" name="param21b" type="string" gui-text="Label string:">String level 0</param>
      <param indent="0" name="param21e" type="boolean" _gui-text="Boolean parameter">true</param>
      <param indent="0" name="param21f" type="enum" _gui-text="Enumeration (default):">
        <_item value="enum21a">Choice 1</_item>
        <_item value="enum21b">Choice 2</_item>
        <_item value="enum21c" default="true">Choice 3</_item>
        <_item value="enum21d">Choice 4</_item>
      </param>
      <param indent="0" name="param21c" type="int" gui-text="Label int:">0</param>
      <param indent="0" name="param21d" type="int" gui-text="Label int:" appearance="full">10</param>
      <!-- fake spacer -->
      <param indent="0" name="param21x" type="description" xml:space="preserve"> </param>
      <!-- fake spacer -->
      <param indent="0" name="param22x" type="description" xml:space="preserve"> </param>
      <!-- header -->
      <param indent="4" name="param23" type="description" appearance="header">Indent Header 4</param>
      <!-- misc -->
      <param indent="4" name="param23a" type="description" xml:space="preserve">Text level 4</param>
      <param indent="4" name="param23b" type="string" gui-text="Label string:">String level 4</param>
      <param indent="4" name="param23e" type="boolean" _gui-text="Boolean parameter">true</param>
      <param indent="4" name="param23f" type="enum" _gui-text="Enumeration (default):">
        <_item value="enum23a">Choice 1</_item>
        <_item value="enum23b">Choice 2</_item>
        <_item value="enum23c" default="true">Choice 3</_item>
        <_item value="enum23d">Choice 4</_item>
      </param>
      <param indent="4" name="param23c" type="int" gui-text="Label int:">4</param>
      <param indent="4" name="param23d" type="int" gui-text="Label int:" appearance="full">6</param>
      <!-- misc -->
    </page>

    <!-- color widget tab -->
    <page name="color_tab" _gui-text="Color">
      <param name="param41" gui-text="Color" type="color">-2147450625</param>
    </page>

    <!-- help tab -->
    <page name="help_tab" _gui-text="Help">
      <_param name="help" type="description" xml:space="preserve">TODO</_param>
    </page>

    <!-- about tab -->
    <page name="about_tab" _gui-text="About">
      <_param name="about" type="description" xml:space="preserve">INX Demo</_param>
    </page>

  </param> <!-- notebook end -->

  <effect needs-live-preview="false" needs-document="false">
    <object-type>all</object-type>
    <effects-menu hidden="false">
      <submenu _name="Debug"/>
    </effects-menu>
  </effect>

  <options silent="true"></options>

</inkscape-extension>
