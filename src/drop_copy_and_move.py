#!/usr/bin/env python
"""
drop_a_copy_and_move - test inconsistency in retained selection with v27

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=invalid-name

# standard library
import copy

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex
from simpletransform import applyTransformToNode
from simpletransform import composeParents, composeTransform, invertTransform
from simpletransform import parseTransform


__version__ = '0.0'


# ----- extended simpletransform.py

# pylint: disable=missing-docstring

def ident_mat():
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def rootToNodeTransform(node, mat=None):
    mat = ident_mat() if mat is None else mat
    if node is not None and node.getparent() is not None:
        return invertTransform(composeParents(node, mat))
    else:
        return mat


def nodeTransform(node):
    if 'transform' in node.attrib:
        return parseTransform(node.get('transform'))
    else:
        return ident_mat()


def composeTransformInRoot(node, center, mat_transform):
    """Compose mat_transform for node applied in SVG root."""
    node_mat = nodeTransform(node)
    root_mat = rootToNodeTransform(node)
    if center is None:
        center_mat = ident_mat()
    else:
        center_mat = [[1.0, 0.0, center[0]],
                      [0.0, 1.0, center[1]]]
    return composeTransform(
        node_mat,
        composeTransform(
            root_mat,
            composeTransform(
                center_mat,
                composeTransform(
                    mat_transform,
                    composeTransform(
                        invertTransform(center_mat),
                        composeTransform(
                            invertTransform(root_mat),
                            invertTransform(node_mat)))))))

# pylint: enable=missing-docstring


# ----- SVG helper functions

def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def make_copy(node, mode="duplicate"):
    """Create a copy of node.

    Return the original or the new node based on *mode*.
    """
    parent = node.getparent()
    index = parent.index(node)
    new_node = copy.deepcopy(node)
    new_node.attrib.pop('id', None)
    if mode == 'drop':
        # insert copy before, return original
        parent.insert(index, new_node)
        return node
    elif mode == 'stamp':
        # insert copy after, return copy
        parent.insert(index+1, new_node)
        return new_node
    else:
        # append copy to parent, return copy
        parent.append(new_node)
        return new_node


# ----- main class

class DropCopyAndMove(inkex.Effect):
    """Effect-based class to debug crash with custom rotation centers."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="drop",
                                     help="Selected action")
        self.OptionParser.add_option("--offset_x",
                                     action="store", type="float",
                                     dest="offset_x", default=30,
                                     help="Offset X")
        self.OptionParser.add_option("--offset_y",
                                     action="store", type="float",
                                     dest="offset_y", default=30,
                                     help="Offset Y")
        self.OptionParser.add_option("--direction",
                                     action="store", type="string",
                                     dest="direction", default="h",
                                     help="Move direction")
        self.OptionParser.add_option("--count",
                                     action="store", type="int",
                                     dest="count", default=1,
                                     help="Number of copies")

    # ----- transform

    def transform(self, mat, node):
        """Apply final transformation matrix to object, or a copy/clone."""
        # pylint: disable=no-self-use
        applyTransformToNode(mat, node)

    def translate(self, i, node_id):
        """Translate object with node_id based on options."""
        copy_mode = self.options.action
        node = make_copy(self.selected[node_id], copy_mode)
        tx = ty = 0.0
        factor = 1
        # offset
        if 'x' in self.options.direction:
            tx = self.options.offset_x
        if 'y' in self.options.direction:
            ty = self.options.offset_y
        # dt to uu
        ty *= -1
        # count offset factor for copies
        if copy_mode != 'drop':
            factor = i + 1
        # translate has no anchor
        anchor = None
        # transform to root, translate, transform back to node
        mat_translate = [[1.0, 0.0, factor * tx],
                         [0.0, 1.0, factor * ty]]
        mat = composeTransformInRoot(node, anchor, mat_translate)
        self.transform(mat, node)

    def effect(self):
        """Main entry point to process current document."""
        # get z-sorted list of selected object ids
        root = self.document.getroot()
        id_list = z_iter(root, self.selected.keys())
        # transform each selected object
        for node_id in id_list:
            for i in reversed(range(self.options.count)):
                self.translate(i, node_id)


if __name__ == '__main__':
    ME = DropCopyAndMove()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
