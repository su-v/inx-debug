#!/usr/bin/env python
"""
delete_objects - test deleting objects with patched inkscape

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=too-many-ancestors

# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


SKIP_CONTAINERS = [
    'defs',
    'glyph',
    'marker',
    'mask',
    'missing-glyph',
    'pattern',
    'symbol',
]
CONTAINER_ELEMENTS = [
    'a',
    'g',
    'switch',
]
GRAPHICS_ELEMENTS = [
    'circle',
    'ellipse',
    'image',
    'line',
    'path',
    'polygon',
    'polyline',
    'rect',
    'text',
    'use',
]


def get_defs(node):
    """Find <defs> in children of *node*, return first one found."""
    path = '/svg:svg//svg:defs'
    try:
        return node.xpath(path, namespaces=inkex.NSS)[0]
    except IndexError:
        return inkex.etree.SubElement(node, inkex.addNS('defs', 'svg'))


def z_sort(node, alist):
    """Return new list sorted in document order (depth-first traversal)."""
    ordered = []
    id_list = list(alist)
    count = len(id_list)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            ordered.append(element_id)
            count -= 1
            if not count:
                break
    return ordered


def z_iter(node, alist):
    """Return iterator over ids in document order (depth-first traversal)."""
    id_list = list(alist)
    for element in node.iter():
        element_id = element.get('id')
        if element_id is not None and element_id in id_list:
            id_list.remove(element_id)
            yield element_id


def iter_graphic_elements(node):
    """Return descendents of node as iterator over of graphics elements."""
    graphics = [inkex.addNS(c, 'svg') for c in GRAPHICS_ELEMENTS]
    for element in node.iter(tag=inkex.etree.Element):
        if element.tag in graphics:
            yield element


def get_graphic_elements(node):
    """Return descendents of node as list of graphics element nodes."""
    return list(iter_graphic_elements(node))


def get_graphic_element_ids(node):
    """Return descendents of node as ID list of graphics elements."""
    return [element.get('id') for element in iter_graphic_elements(node)]


class DeleteObjects(inkex.Effect):
    """Effect-based class to debug deleting existing objects."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--mode",
                                     action="store", type="string",
                                     dest="mode", default="delete_top",
                                     help="Selected action")

    def iter_inverted_selection(self):
        """Return iter over ids not part of current selection."""
        selected = self.selected.keys()
        parents = set()
        inverted = set()
        for node in self.selected.values():
            parents.update([node.getparent()])
        for node in parents:
            inverted.update(get_graphic_element_ids(node))
        for id_ in inverted:
            if id_ in selected:
                selected.remove(id_)
            else:
                yield id_

    def get_inverted_selection(self):
        """Return list of ids not part of current selection."""
        return list(self.iter_inverted_selection())

    def get_siblings(self, id_list):
        """Return sibling ids for each element listed in id_list."""
        siblings = set()
        for node_id in id_list:
            node = self.getElementById(node_id)
            for sibling in node.itersiblings(preceding=True):
                sibl_id = sibling.get('id')
                if sibl_id not in id_list:
                    siblings.update([sibl_id])
            for sibling in node.itersiblings(preceding=False):
                sibl_id = sibling.get('id')
                if sibl_id not in id_list:
                    siblings.update([sibl_id])
        return siblings

    def delete_objects(self, id_list):
        """Delete objects with id in id_list."""
        for node_id in id_list:
            node = self.getElementById(node_id)
            if node is not None:
                node.getparent().remove(node)
                inkex.debug("Deleted object with id: {}".format(node_id))

    def effect(self):
        """Main entry point to process current document."""
        root = self.document.getroot()
        id_list = z_sort(root, self.selected.keys())
        if self.options.mode == 'delete_top':
            delete_ids = [id_list.pop()]
        elif self.options.mode == 'delete_bottom':
            delete_ids = [id_list[0]]
        elif self.options.mode == 'delete_selected':
            delete_ids = list(id_list)
        elif self.options.mode == 'delete_not_selected':
            delete_ids = self.get_siblings(id_list)
        self.delete_objects(delete_ids)


if __name__ == '__main__':
    ME = DeleteObjects()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
