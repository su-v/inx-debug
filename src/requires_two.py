#!/usr/bin/env python
"""
requires_two - debug retaining of selection if extension "fails"

Copyright (C) 2017, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# local library
try:
    import inkex_local as inkex
except ImportError:
    import inkex


__version__ = '0.0'


# ----- main class

class TwoRequired(inkex.Effect):
    """Effect-based class to debug retaining of selection on failure."""

    def __init__(self):
        """Init base class."""
        inkex.Effect.__init__(self)

        # options
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default="check_selection",
                                     help="Selected action")
        self.OptionParser.add_option("--count",
                                     action="store", type="int",
                                     dest="count", default=2,
                                     help="Number of required objects")

    def effect(self):
        """Main entry point to process current document."""
        id_list = self.options.ids
        if self.options.action == 'check_selection':
            if len(id_list) < self.options.count:
                inkex.debug("This extension requires two or more "
                            "selected objects.")
            elif len(id_list) == 2:
                # do nothing
                pass
            elif len(id_list) > 2:
                # force reload (send document back to inkscape)
                root = self.document.getroot()
                root.set('id', self.uniqueId('SVGRoot'))


if __name__ == '__main__':
    ME = TwoRequired()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
